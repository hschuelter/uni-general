#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes.h"

extern dArvore* tabsimb;
extern int posicaoMemoria;
extern int linha;
extern int l1;
extern int flag;

extern FILE* file;
extern char* nomeArq;


dLista* criaLista(char* id){
    dLista* descritor;

    if( ! (descritor = (dLista*) malloc( sizeof(dLista) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    if( ! (descritor->inicio = (NoLista*) malloc( sizeof(NoLista) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    descritor->fim = descritor->inicio;

    strncpy(descritor->inicio->id,id,MAXID);
    descritor->inicio->proximo = NULL;
    descritor->inicio->anterior = NULL;

    return descritor;
}

dArvore* criaArvore(NoArvore* no){
    dArvore* descritor;
    NoArvore* raiz = NULL;

    if( ! (descritor = (dArvore*) malloc( sizeof(dArvore) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    if( ! (descritor->raiz = (NoArvore*) malloc( sizeof(NoArvore) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    raiz = descritor->raiz;

    strncpy(raiz->id,no->id, MAXID);
    raiz->tipo = no->tipo;
    raiz->esq=NULL;
    raiz->dir=NULL;

    return descritor;
}

NoAST* criaFolhaID(char* id){
    NoAST* folha = NULL;

    if( ! (folha = (NoAST*) malloc( sizeof(NoAST) )) ){
        printf("Deu ruim no malloc\n");
        exit(EXIT_FAILURE);
    }

    if( (folha->noArvore = verificaVariavel(tabsimb,id)) == NULL){
    	printf("Erro: variavel %s nao declarada! (linha %d)\n",id,linha);
    	exit(EXIT_FAILURE);
    }

    folha->cod = VAR;
    folha->cast = folha->noArvore-> tipo;

    folha->esq = NULL;
    folha->dir = NULL;
    strncpy(folha->id, id, MAXID);
    
    /*
    printf("%s: %d\n",folha->id,folha->cast);
    printf("dst: >%s<\n",folha->id);
    printf("src: >%s<\n",id);*/
    


    return folha;
}


NoAST* criaFolhaIDEquals(char* id){
    NoAST* folha = NULL;

    if( ! (folha = (NoAST*) malloc( sizeof(NoAST) )) ){
        printf("Deu ruim no malloc\n");
        exit(EXIT_FAILURE);
    }

    if( (folha->noArvore = verificaVariavel(tabsimb,id)) == NULL){
    	printf("Nao existe tal variavel: %s\n",id);
    	exit(EXIT_FAILURE);
    }

    folha->cod = VAR_EQUALS;
    folha->cast = folha->noArvore-> tipo;

    folha->esq = NULL;
    folha->dir = NULL;
    strncpy(folha->id, id, MAXID);

    
    /*
    printf("dst: >%s<\n",folha->id);
    printf("src: >%s<\n",id);
    */


    return folha;
}

NoAST* criaFolhaInt(int consInt){
    NoAST* folha = NULL;

    if( ! (folha = (NoAST*) malloc( sizeof(NoAST) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    folha->cod = CON_INT;
    folha->cast = T_INT;

    folha->consInt = consInt;
    folha->esq = NULL;
    folha->dir = NULL;

    return folha;
}

NoAST* criaFolhaFloat(float consFloat){
    NoAST* folha = NULL;

    if( ! (folha = (NoAST*) malloc( sizeof(NoAST) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    folha->cod = CON_FLOAT;
    folha->cast = T_FLOAT;

    folha->consFloat = consFloat;
    folha->esq = NULL;
    folha->dir = NULL;

    return folha;
}


NoAST*  criaFolhaString(char* consString){
    NoAST* folha = NULL;

    if( ! (folha = (NoAST*) malloc( sizeof(NoAST) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    folha->cod  = CON_STRING;
    folha->cast = T_STRING;

    strncpy(folha->consString, consString, strlen(consString) );
    folha->esq = NULL;
    folha->dir = NULL;

    return folha;
}


NoAST* criaNoAST(int operacao, NoAST* esquerda, NoAST* direita){
    NoAST* NoRet;

    NoRet = verificaTipo(operacao, esquerda,direita);

    NoRet->cod = operacao;

    return NoRet;
}

NoAST* criaNoAST_IfElse(int operacao, NoAST* esquerda, NoAST* direita, NoAST* ptr3){
    NoAST* NoRet;


    NoRet = verificaTipo(operacao, esquerda, direita);
    NoRet->ptr3 = ptr3;
    NoRet->cod = operacao;

    return NoRet;
}


void insereNaLista(dLista* desc, char* id){
    NoLista* no;
    NoLista* aux;

    if( ! (no = (NoLista*) malloc( sizeof(NoLista) )) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    strncpy(no->id, id,MAXID);
    no->proximo = NULL;
    no->anterior = NULL;

    aux = desc->fim;
    aux->proximo = no;
    no->anterior = aux;
    aux = aux->proximo;

    desc->fim = aux;
    if(desc->inicio == NULL)
        desc->inicio = aux;

    aux = NULL;
}

dArvore* insereNaArvore(dLista* dLista, int tipo, dArvore* dArvore){
    NoArvore* no;
    NoArvore* aux;
    NoArvore* iterador;
    NoLista* nolista;

    //printf("Vou inserir na Arvore variaveis do tipo: %d\n", tipo);


    nolista = dLista->inicio;
    /*
    printf("Fila:");
    imprimeLista(dLista);
    */
    while(dLista->inicio != NULL){

        if(dLista->inicio == NULL)
            break;

        if( ! (no = (NoArvore*) malloc( sizeof(NoArvore) )) ){
            printf("Deu ruim no malloc\n");
            exit(1);
        }
        strncpy(no->id, dLista->inicio->id, MAXID);

        no->tipo = tipo;
        //no->valor = valor;
        no->posicaoMemoria = posicaoMemoria++;
        no->dir = NULL;
        no->esq = NULL;

        if(dArvore == NULL){
            dArvore = criaArvore(no);
            iterador = dArvore->raiz;
        }
        else{

            iterador =  dArvore->raiz;

            while(no != NULL){
                //printf(">%s< >%s< ",iterador->id, no->id);
                if(strcmp(no->id, iterador->id) > 0){
                    if(iterador->dir == NULL){
                        //printf(" coloca na direita\n");
                        iterador->dir = no;
                        no = NULL;
                    }

                    else{
                        //printf(" vai para a direita\n");
                        iterador = iterador->dir;
                    }

                }
                else if( strcmp(no->id, iterador->id) < 0 ){

                    if(iterador->esq == NULL){
                        //printf(" coloca na esquerda\n");
                        iterador->esq = no;
                        no = NULL;
                    }

                    else{
                        //printf(" vai para a esquerda\n");
                        iterador = iterador->esq;
                    }

                }

                else if( strcmp(no->id, iterador->id) == 0 ){
                    //printf("iguais!\n");
                    no = NULL;
                }
            }
        }

        removeDaLista(dLista);
        aux = NULL;
    }
    /*
    printf("Tabela:\n");
    imprimeEmOrdem(dArvore->raiz);
    printf("\nFila:");
    imprimeLista(dLista);
    */
    //numVar++;
    return dArvore;
}

void removeDaLista(dLista* desc){
    NoLista* aux = NULL;
    int comparacao = 0;

    //printf("Estou removendo da lista\n");

    aux = desc->inicio;

    if(aux->proximo != NULL){
        desc->inicio = aux->proximo;
        desc->inicio->anterior = NULL;
    }
    else{
        desc->inicio = NULL;
    }

    aux->proximo = NULL;
    aux->anterior = NULL;

    free(aux);
}


/* IMPRESSÕES */
void imprimeLista(dLista* desc){
    NoLista* aux = NULL;

    aux = desc->inicio;

    printf("\nInicio da Fila:\t%s\n", aux->id);
    if(aux != NULL){
        while(aux->proximo != NULL){
            aux = aux->proximo;
            if(desc->fim == aux)
                printf("Fim da fila:\t%s\n\n", aux->id);
            else
                printf("\t\t%s\n", aux->id);
        }
    }
    printf("\n");
}

void imprimeTabela(NoArvore* noArvore){
    int caso;

    if (noArvore == NULL)
      return;

    imprimeTabela(noArvore->esq);


    printf("|%-16s|\t %-9d|",noArvore->id, noArvore->posicaoMemoria);
    caso = noArvore->tipo;

    switch(caso){
        case 1:
            printf("\t int\t   |\n");
            break;

        case 2:
            printf("\tfloat\t   |\n");
            break;

        case 3:
            printf("\tstring\t   |\n");
            break;

        default:
            printf("\t(?)\n");
            break;
    }

    imprimeTabela(noArvore->dir);
}

void imprimeHeaderTabela(){
    printf("\n");
    printf("----------------------------------------------------\n");
    printf("|    Variavel    |     Quadro     |      Tipo      |\n");
    printf("----------------------------------------------------\n");
}


void imprimeHeader(FILE* file, char* str){
    char* nomeClasse;
    int len = strlen(str);

    nomeArq = (char*) malloc( len - 4 );
    nomeArq[0] = '\0';
    strncpy(nomeArq, str, len - 4 );

    nomeClasse = (char*) malloc( len - 4 );
    strncpy(nomeClasse, nomeArq, strlen(nomeArq));
    nomeClasse[strlen(nomeClasse)+1] = '\0';

    strcat(nomeArq, ".class");

    file = fopen(nomeArq, "w");

    fprintf(file, ".class public %s\n", nomeClasse);
    fprintf(file, ".super java/lang/Object\n\n");

    fprintf(file, ".method pulbic <init>()V\n");
    fprintf(file, "\taload_0\n\n");
    fprintf(file, "\tinvokenonvirtual java/lang/Object/<init>()V\n");
    fprintf(file, "\treturn\n");
    fprintf(file, ".end method\n\n");

    fprintf(file, ".method public static main([Ljava/lang/String;)V\n");
    fprintf(file, "\t.limit stack 8\n");
    //fprintf(file, "\t.limit locals %d\n\n", numVar);

    fclose(file);
    
}

void imprimeVariaveis(FILE* file, int num){
    
    file = fopen(nomeArq, "a");
    fprintf(file, "\t.limit stack %d\n\n", num);
    fclose(file);
}

void imprimeFinal(FILE* file){
    file = fopen(nomeArq, "a");

    fprintf(file, "\treturn\n\n");
    fprintf(file, ".end method\n");
}

/* GERAÇÃO DE CÓDIGO */

void gerarCodigo(NoAST* noAST){
	int caso;
	int lTrue, lFalse;
	int laux;
    //NoAST* aux;

    if(noAST == NULL){
        //printf("\t>>NULO<<\n\n");
        return;
    }

    caso = noAST->cod;
    
    switch(caso){

    	case VAR:
            file = fopen(nomeArq, "a");
            switch(noAST->noArvore->tipo){
                case T_INT:     fprintf(file, "\tiload %d\n", noAST->noArvore->posicaoMemoria); break;
                case T_FLOAT:   fprintf(file, "\tfload %d\n", noAST->noArvore->posicaoMemoria); break;
                case T_STRING:  fprintf(file, "\tldc %d\n",   noAST->noArvore->posicaoMemoria); break;
                default:        break;
            }
            fclose(file);
            
            break;

        case CON_INT:
            file = fopen(nomeArq, "a");

        	if(noAST->consInt <= 5 && noAST->consInt >= 0)
            	fprintf(file, "\ticonst_%d\n", noAST->consInt);
            
            else if(noAST->consInt < 128 || noAST->consInt > -128)
            	fprintf(file, "\tbi_push %d\n", noAST->consInt);
            
            else 
            	fprintf(file, "\tldc %d\n", noAST->consInt);
            
            fclose(file);
            break;

        case CON_FLOAT:
            file = fopen(nomeArq, "a");	 fprintf(file, "\tldc %.2f\n", noAST->consFloat);    fclose(file);  break;

        case CON_STRING:
            file = fopen(nomeArq, "a"); fprintf(file, "\tldc %s\n", noAST->consString); fclose(file);  break;

        case ADD:

        	gerarCodigo(noAST->esq);
        	gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
            switch(noAST->cast){
                case T_INT:     fprintf(file,"\tiadd\n"); break;
                case T_FLOAT:   fprintf(file,"\tfadd\n"); break;
                default:        break;
            }
            fclose(file);

            break;


        case DIF:

            gerarCodigo(noAST->esq);
            gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
            switch(noAST->cast){
                case T_INT:     fprintf(file,"\tisub\n"); break;
                case T_FLOAT:   fprintf(file,"\tfsub\n"); break;
                default:        break;
            }
            fclose(file);

            break;


        case MUL:
            
            gerarCodigo(noAST->esq);
            gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
            switch(noAST->cast){
                case T_INT:     fprintf(file,"\timul\n"); break;
                case T_FLOAT:   fprintf(file,"\tfmul\n"); break;
                default:        break;
            }
            fclose(file);

            break;


        case DIV:

            gerarCodigo(noAST->esq);
            gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
            switch(noAST->cast){
                case T_INT:     fprintf(file,"\tidiv\n"); break;
                case T_FLOAT:   fprintf(file,"\tfdiv\n"); break;
                default:        break;
            }
            fclose(file);

            break;

        case MINUN:

            gerarCodigo(noAST->esq);
            gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
        	switch(noAST->cast){
                case T_INT:     fprintf(file,"\tineg\n"); break;
                case T_FLOAT:   fprintf(file,"\tfneg\n"); break;
                default:        break;
            }
            fclose(file);

            break;

        case EQUALS:

            gerarCodigo(noAST->esq);

            file = fopen(nomeArq, "a");
            switch(noAST->dir->noArvore->tipo){
                case T_INT:     fprintf(file,"\tistore %d\n",noAST->dir->noArvore->posicaoMemoria); break;
                case T_FLOAT:   fprintf(file,"\tfstore %d\n",noAST->dir->noArvore->posicaoMemoria); break;
                case T_STRING:  fprintf(file,"\tstore on %s\n",noAST->dir->noArvore->id);           break;
                default:        break;
            }
            fclose(file);
            
        	break;

        

        case ITOFLOAT:

        	gerarCodigo(noAST->esq);

            file = fopen(nomeArq, "a");
        	fprintf(file, "\ti2f\n");
            fclose(file);
        	break;

        case FTOINT:

            gerarCodigo(noAST->esq);
            file = fopen(nomeArq, "a");

            fprintf(file, "\tf2i\n");
            
            fclose(file);
            break;

        case LISTA:
        	gerarCodigo(noAST->esq);
        	gerarCodigo(noAST->dir);

        	break;

        /* TERMINAR A IMPRESSÃO */
        
        case IF:
        	lTrue = newLabel();
        	lFalse= newLabel();

        	gerarExLogRel(noAST->esq, lTrue, lFalse);

            file = fopen(nomeArq, "a");
        	fprintf(file, "L%d:\n", lTrue);
            fclose(file);

        	gerarCodigo(noAST->dir);
        	
            file = fopen(nomeArq, "a");
            fprintf(file, "L%d:\n", lFalse);
            fclose(file);

        	break;

       	case IFELSE:
       		lTrue = newLabel();
       		lFalse= newLabel();

       		gerarExLogRel(noAST->esq, lTrue, lFalse);
            
            file = fopen(nomeArq, "a");
        	fprintf(file, "L%d:\n", lTrue);
            fclose(file);

        	gerarCodigo(noAST->dir);
        	laux = newLabel();

            file = fopen(nomeArq, "a");
            fprintf(file, "\tgoto\t  L%d\n",laux);
        	fprintf(file, "L%d:\n", lFalse);
            fclose(file);

        	gerarCodigo(noAST->ptr3);
        	
            file = fopen(nomeArq, "a");
            fprintf(file, "L%d:\n", laux);
            fclose(file);

       		break;

       	case WHILE:

       		laux  = newLabel();
       		lTrue = newLabel();
       		lFalse= newLabel();


            file = fopen(nomeArq, "a");
        	fprintf(file, "L%d:\n", laux);
            fclose(file);

        	gerarExLogRel(noAST->esq, lTrue, lFalse);
			
            file = fopen(nomeArq, "a");
            fprintf(file, "L%d:\n", lTrue);
            fclose(file);

			gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
			fprintf(file, "\tgoto\t  L%d\n",laux);
        	fprintf(file, "L%d:\n", lFalse);
            fclose(file);

       		break;

        case PRINT:

            file = fopen(nomeArq, "a");
            fprintf(file, "\tgetstatic java/lang/System.out Ljava/io/PrintStream\n");
            fclose(file);

            gerarCodigo(noAST->esq);
            
            file = fopen(nomeArq, "a");
            switch(noAST->cast){
                case T_INT:     fprintf(file, "\tinvokevirtual java/io/PrintStream/println(I)\n"); break;
                case T_FLOAT:   fprintf(file, "\tinvokevirtual java/io/PrintStream/println(F)\n"); break;
                case T_STRING:  fprintf(file, "\tinvokevirtual java/io/PrintStream/println([Ljava/lang/String;)\n"); break;
            }
            fclose(file);
            break;

        default:
        	printf("\tNot implemented yet...\n");
        	break;
    }

}

void gerarExLogRel(NoAST* noAST, int lTrue, int lFalse){
	int laux;

	if(noAST == NULL){
		return;
	}

	switch(noAST->cod){
		
		case MENOR:

			gerarCodigo(noAST->esq);
			gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");

			switch(noAST->cast){
                case T_INT:     fprintf(file, "\tif_icmplt L%d\n",lTrue); break;
                case T_FLOAT:   fprintf(file, "\tif_fcmplt L%d\n",lTrue); break;
                default:        break;
            }
            fprintf(file, "\tgoto\t  L%d\n",lFalse);

            fclose(file);

			break;

		case MENORIG:

			gerarCodigo(noAST->esq);
			gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
			switch(noAST->cast){
                case T_INT:     fprintf(file, "\tif_icmple L%d\n",lTrue); break;
                case T_FLOAT:   fprintf(file, "\tif_fcmple L%d\n",lTrue); break;
                default:        break;
            }
            fprintf(file, "\tgoto\t  L%d\n",lFalse);
            fclose(file);

			break;

		case MAIOR:

			gerarCodigo(noAST->esq);
			gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
			switch(noAST->cast){
                case T_INT:     fprintf(file, "\tif_icmpgt L%d\n",lTrue); break;
                case T_FLOAT:   fprintf(file, "\tif_fcmpgt L%d\n",lTrue); break;
                default:        break;
            }
            fprintf(file, "\tgoto\t  L%d\n",lFalse);
            fclose(file);

			break;

		case MAIORIG:

			gerarCodigo(noAST->esq);
			gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
			switch(noAST->cast){
                case T_INT:     fprintf(file, "\tif_icmpge L%d\n",lTrue); break;
                case T_FLOAT:   fprintf(file, "\tif_fcmpge L%d\n",lTrue); break;
                default:        break;
            }
            fprintf(file, "\tgoto\t  L%d\n",lFalse);
            fclose(file);

			break;

		case IGUAL:

			gerarCodigo(noAST->esq);
			gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
			switch(noAST->cast){
                case T_INT:     fprintf(file, "\tif_icmpeq L%d\n",lTrue); break;
                case T_FLOAT:   fprintf(file, "\tif_fcmpeq L%d\n",lTrue); break;
                default:        break;
            }
            fprintf(file, "\tgoto\t  L%d\n",lFalse);
            fclose(file);

			break;

		case DIFERENTE:

			gerarCodigo(noAST->esq);
			gerarCodigo(noAST->dir);

            file = fopen(nomeArq, "a");
			switch(noAST->cast){
                case T_INT:     fprintf(file, "\tif_icmpne L%d\n",lTrue); break;
                case T_FLOAT:   fprintf(file, "\tif_fcmpne L%d\n",lTrue); break;
                default:        break;
            }
            fprintf(file, "\tgoto\t  L%d\n",lFalse);
            fclose(file);

			break;

		case AND:
			laux = newLabel();

			gerarExLogRel(noAST->esq, laux, lFalse);

            file = fopen(nomeArq, "a");
			printf("L%d:\n",laux);
            fclose(file);
			
            gerarExLogRel(noAST->dir, lTrue, lFalse);;

			break;

		case OR:
			laux = newLabel();

			gerarExLogRel(noAST->esq, lTrue, laux);

            file = fopen(nomeArq, "a");
			printf("L%d:\n",laux);
            fclose(file);

			gerarExLogRel(noAST->dir, lTrue, lFalse);;

			break;

		case NOT:
			gerarExLogRel(noAST->esq, lFalse, lTrue);
			break;

		default:
			break;
	}

}

/* GERAÇÃO DE CÓDIGO */


NoArvore* verificaVariavel(dArvore* descritor, char* id){
	NoArvore* aux = NULL;
	aux = descritor->raiz;

	while(aux != NULL){
		if(strcmp(id, aux->id) > 0){
			aux = aux->dir;
		}


		else if(strcmp(id, aux->id) < 0){
			aux = aux->esq;
		}

		else{
			return aux;
		}
	}

	return NULL;
}


NoAST* verificaTipo(int operacao, NoAST* esquerda, NoAST* direita){
    NoAST* NoRet;
    NoAST* NoConv;
    int cast;

    /* VERIFICAÇÃO DO MALLOC */
    if( !(NoRet = (NoAST*) malloc(sizeof(NoAST))) ){
        printf("Deu ruim no malloc\n");
        exit(1);
    }

    /* INÍCIO DAS VERIFICAÇÕES */

    if(operacao == LISTA || operacao == IF || operacao == IFELSE || operacao == WHILE){
    	NoRet->cast = -1;
    	NoRet->esq = esquerda;
    	NoRet->dir = direita;

    	return NoRet;
    }

    else if(operacao == NOT || operacao == PRINT){

        cast = esquerda->cast;
        
        NoRet->cast = cast;
        NoRet->esq = esquerda;
        NoRet->dir = NULL;

        return NoRet;

    }

    else if(operacao == EQUALS){

        if(esquerda->cast != direita->cast){

            if(esquerda->cast == T_STRING || direita->cast == T_STRING){
                printf("Erro: operacao entre String e numero!\n");
                printf("\t (linha: %d)\n",linha);
                exit(1);
            }

            else if(direita->cast == T_INT && esquerda->cast == T_FLOAT){
                
                if( !(NoConv = (NoAST*) malloc(sizeof(NoAST))) ){
                    printf("Deu ruim no malloc\n");
                    exit(1);
                }

                cast = T_INT;

                NoConv->cod  = FTOINT;
                NoConv->esq  = esquerda;
                NoConv->dir  = NULL;
                NoConv->cast = cast;

                NoRet->cast = cast;
                NoRet->esq  = NoConv;
                NoRet->dir  = direita;


                printf("Warning: Conversao de float para int, possivel perda de informacao!\n");
                printf("\t (linha %d)\n\n",linha);

                return NoRet;
            }

            else if(direita->cast == T_FLOAT && esquerda->cast == T_INT){
                
                if( !(NoConv = (NoAST*) malloc(sizeof(NoAST))) ){
                    printf("Deu ruim no malloc\n");
                    exit(1);
                }

                cast = T_FLOAT;

                NoConv->cod  = ITOFLOAT;
                NoConv->esq  = esquerda;
                NoConv->dir  = NULL;
                NoConv->cast = cast;

                NoRet->cast = cast;
                NoRet->esq  = NoConv;
                NoRet->dir  = direita;

                return NoRet;
            }

        }
        else{     
            cast = max(esquerda->cast,direita->cast);

            NoRet->cast = cast;
            NoRet->esq  = esquerda;
            NoRet->dir  = direita;


            return NoRet;

        }
    }

    else if(operacao == MINUN){

        cast = esquerda->cast;

        NoRet->cast = cast;
        NoRet->esq  = esquerda;
        NoRet->dir  = NULL;

        return NoRet;
    }

    else if(esquerda->cast != direita->cast){
    	
    	/* VERIFICAÇÃO PARA STRINGS */

        if(esquerda->cast == T_STRING || direita->cast == T_STRING){
        	printf("Nao eh possivel realizar operacoes com strings!\n");
            printf("\t (linha: %d)\n",linha);
        	exit(1);
        }

        /* CONVERTER O INT(ESQUERDA) PARA FLOAT */

        else if(esquerda->cast == T_INT){

            if( !(NoConv = (NoAST*) malloc(sizeof(NoAST))) ){
                printf("Deu ruim no malloc\n");
                exit(1);
            }
            cast = T_FLOAT;

            NoConv->cod  = ITOFLOAT;
            NoConv->esq  = esquerda;
            NoConv->dir  = NULL;
            NoConv->cast = cast;

            NoRet->cast = cast;
            NoRet->esq  = NoConv;
            NoRet->dir  = direita;

            return NoRet;
        }

        /* CONVERTTER O INT(DIREITA) PARA FLOAT */

        else if(direita->cast == T_INT){

            if( !(NoConv = (NoAST*) malloc(sizeof(NoAST))) ){
                printf("Deu ruim no malloc\n");
                exit(1);
            }
            cast = T_FLOAT;

            NoConv->cod  = ITOFLOAT;
            NoConv->esq  = direita;
            NoConv->dir  = NULL;
            NoConv->cast = cast;

            NoRet->cast = cast;
            NoRet->esq  = esquerda;
            NoRet->dir  = NoConv;

            return NoRet;
        }

    }

    else{        
        cast = esquerda->cast;

        NoRet->cast = cast;
        NoRet->esq  = esquerda;
        NoRet->dir  = direita;


        return NoRet;

    }
}

int newLabel(){
	return l1++;
}

int max(int a, int b){

	return (a > b) ? a : b;
}

int min(int a, int b){

	return (a < b) ? a : b;
}

void swap(int* a, int* b){
	int aux;

	aux = *a;
	*a = *b;
	*b = aux;
}
