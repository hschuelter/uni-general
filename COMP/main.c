#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes.h"

extern FILE *yyin;
extern dArvore* tabsimb;
extern FILE* file;
extern char* nomeArq;
extern int numVar;

void yyparse();

int main(int argc, char* argv[]){
	// gcc -o comp main.c expr.tab.c
	
	nomeArq = (char*) malloc( strlen(argv[1]) );
	strncpy(nomeArq, argv[1], strlen(argv[1]));

	/*aux = (char*) malloc(strlen(argv[1]) + 3);
	strcat(aux, "aux");*/
	imprimeHeader(file, argv[1]);
	
	yyin = fopen(argv[1],"r");
	yyparse();

	imprimeFinal(file);

	
	imprimeHeaderTabela();
	imprimeTabela(tabsimb->raiz);	
	printf("----------------------------------------------------\n");
	printf("\nRaiz: %s\n",tabsimb->raiz->id);
	
	return 0;

}
