%{
#include <stdio.h>
#include <stdlib.h>
#include "funcoes.h"
#define YYSTYPE Atributo

int linha = 1;
int posicaoMemoria = 0;
int l1=0;
int numVar=0;
int flag=0;

FILE* file;
char* nomeArq;

dArvore* tabsimb;

extern char* argv[];

%}

%token TIGUAL TDIF TAND TOR TMAIORIG TMENORIG
%token TIF TELSE TID TRET TWHILE TSCAN TPRINT
%token TFLOAT TINT TSTRING TVOID
%token TVAR_INT TVAR_FLOAT TVAR_STRING

%%
Programa
    : ListaFuncoes BlocoPrincipal
    | BlocoPrincipal				{printf("\n"); gerarCodigo($1.noAST);}
    ;

ListaFuncoes
    : ListaFuncoes Funcao
    | Funcao
    ;

Funcao
    : TipoRetorno TID '(' DeclParametros ')' BlocoPrincipal
    | TipoRetorno TID '(' ')' BlocoPrincipal
    ;

TipoRetorno
    : Tipo
    | TVOID
    ;

DeclParametros
    : DeclParametros ',' Parametro
    | Parametro
    ;

Parametro
    : Tipo TID
    ;

BlocoPrincipal
    : '{' Declaracoes ListaCmd '}'		{imprimeVariaveis(file, numVar); $$.noAST = $3.noAST;}
    | '{' ListaCmd '}'					{$$.noAST = $2.noAST;}
    ;

Declaracoes
    : Declaracoes Declaracao
    | Declaracao
    ;

Declaracao
    : Tipo ListaId ';'  {tabsimb = insereNaArvore($2.lista, $$.tipo, tabsimb);}
    ;

Tipo
    : TINT      {$$.tipo = T_INT;}
    | TFLOAT    {$$.tipo = T_FLOAT;}
    | TSTRING   {$$.tipo = T_STRING;}
    ;

ListaId
    : ListaId ',' TID   {insereNaLista($$.lista, $3.id); numVar++;}
    | TID               {$$.lista = criaLista($1.id); numVar++;}
    ;

Bloco
    : '{' ListaCmd '}'			{$$.noAST = $2.noAST;}
    | '{' '}'
    ;

ListaCmd
    : ListaCmd Comando			{$$.noAST = criaNoAST(LISTA, $1.noAST, $2.noAST);}
    | Comando					{$$.noAST = $1.noAST;}
    ;

Comando
    : CmdSe 					{$$.noAST = $1.noAST;}
    | CmdEnquanto				{$$.noAST = $1.noAST;}
    | CmdAtrib					{$$.noAST = $1.noAST;}
    | CmdEscrita                {$$.noAST = $1.noAST;}
    | CmdLeitura
    | ChamadaProc
    | Retorno
    ;

Retorno
    : TRET ExpArit ';'
    | TRET TVAR_STRING ';'
    ;

CmdSe
    : TIF '(' ExpLog ')' Bloco          	{$$.noAST = criaNoAST(IF, $3.noAST, $5.noAST);}
    | TIF '(' ExpLog ')' Bloco TELSE Bloco 	{$$.noAST = criaNoAST_IfElse(IFELSE, $3.noAST, $5.noAST, $7.noAST);}
    ;

CmdEnquanto
    : TWHILE '(' ExpLog ')' Bloco 			{$$.noAST = criaNoAST(WHILE, $3.noAST, $5.noAST);}
    ;

CmdAtrib
    : TID '=' ExpArit ';'       {$1.noAST = criaFolhaIDEquals($1.id);
    						     $$.noAST = criaNoAST(EQUALS, $3.noAST, $1.noAST);}
    | TID '=' TVAR_STRING ';'   {$1.noAST = criaFolhaIDEquals($1.id);
                                 $3.noAST = criaFolhaString($3.consString);
                                 $$.noAST = criaNoAST(EQUALS, $3.noAST, $1.noAST);}
    ;

CmdEscrita
    : TPRINT '(' ExpArit ')' ';'        {$$.noAST = criaNoAST(PRINT, $3.noAST, NULL);}
    | TPRINT '(' TVAR_STRING ')' ';'    {$3.noAST = criaFolhaString($3.consString);
                                         $$.noAST = criaNoAST(PRINT, $3.noAST, NULL);}
    ;

CmdLeitura
    : TSCAN '(' TID ')' ';'
    ;

ChamadaProc
    : ChamaFuncao ';'
    ;

ChamaFuncao
    : TID '(' ListaParametros ')'
    | TID '(' ')'
    ;

ListaParametros
    : ListaParametros ',' ExpArit
    | ListaParametros ',' TVAR_STRING
    | ExpArit
    | TVAR_STRING
    ;

ExpLog
    : ExpLog TAND TerLog    {$$.noAST = criaNoAST (AND, $1.noAST, $3.noAST);}
    | ExpLog TOR TerLog     {$$.noAST = criaNoAST (OR, $1.noAST, $3.noAST);}
    | TerLog                {$$.noAST = $1.noAST;}
    ;

TerLog
    : '(' ExpLog ')'		{$$.noAST = $2.noAST;}
    | '!' TerLog            {$$.noAST = criaNoAST (NOT, $2.noAST, NULL);}
    | ExpRel                {$$.noAST = $1.noAST;}
    ;

ExpRel
    : ExpArit '<' ExpArit       {$$.noAST = criaNoAST(MENOR, $1.noAST, $3.noAST);}
    | ExpArit '>' ExpArit       {$$.noAST = criaNoAST(MAIOR, $1.noAST, $3.noAST);}
    | ExpArit TMENORIG ExpArit  {$$.noAST = criaNoAST(MENORIG, $1.noAST, $3.noAST);}
    | ExpArit TMAIORIG ExpArit  {$$.noAST = criaNoAST(MAIORIG, $1.noAST, $3.noAST);}
    | ExpArit TIGUAL ExpArit    {$$.noAST = criaNoAST(IGUAL, $1.noAST, $3.noAST);}
    | ExpArit TDIF ExpArit      {$$.noAST = criaNoAST(DIFERENTE, $1.noAST, $3.noAST);}
    ;

ExpArit
	: ExpArit '+' Termo {$$.noAST = criaNoAST(ADD, $1.noAST, $3.noAST);}
    | ExpArit '-' Termo {$$.noAST = criaNoAST(DIF, $1.noAST, $3.noAST);}
    | Termo             {$$.noAST = $1.noAST;}
	;

Termo
	: Termo '*' Fator   {$$.noAST = criaNoAST(MUL, $1.noAST, $3.noAST);}
	| Termo '/' Fator   {$$.noAST = criaNoAST(DIV, $1.noAST, $3.noAST);}
	| Fator             {$$.noAST = $1.noAST;}
	;

Fator
	: TVAR_INT          {$$.noAST = criaFolhaInt($1.consInt);}
	| TVAR_FLOAT        {$$.noAST = criaFolhaFloat($1.consFloat);}
    | ChamaFuncao       {$$.noAST = criaFolhaID($1.id);}
    | TID               {$$.noAST = criaFolhaID($1.id);}
    | '(' ExpArit ')'   {$$.noAST = $2.noAST;}
    | '-' Fator			{$$.noAST = criaNoAST(MINUN, $2.noAST, NULL);}
	;

%%
#include "lex.yy.c"

int yyerror (char *str)
{
	printf("(%s) antes %s (linha %d)\n", str, yytext, linha);

}

int yywrap()
{
	return 1;
}
