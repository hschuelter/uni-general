#define MAXID       16

#define T_INT       1
#define T_FLOAT     2
#define T_STRING    3

#define VAR         0
#define ADD         1
#define DIF         2
#define MUL         3
#define DIV         4
#define MINUN		5
#define EQUALS		6
#define VAR_EQUALS	7
#define CON_INT     8
#define CON_FLOAT   9
#define CON_STRING  10
#define	ITOFLOAT	11
#define FTOINT      12

#define MENOR       15
#define MENORIG     16
#define MAIOR       17
#define MAIORIG     18
#define IGUAL       19
#define DIFERENTE   20
#define AND         21
#define OR          22
#define NOT 	    23
#define IF 			24
#define IFELSE		25
#define WHILE		26

#define LISTA 		30
#define PRINT       40


typedef struct arvore{
    char id[MAXID];
    int tipo;
    int posicaoMemoria;
    void* valor;
    struct arvore *esq, *dir;
}NoArvore;

typedef struct lista{
    char id[MAXID + 1];
    struct lista *proximo, *anterior;
}NoLista;

typedef struct AST{
    int     cod;
    int 	cast;
    struct AST *esq,*dir, *ptr3;

    NoArvore* noArvore;

    char    id[MAXID];
    int     consInt;
    float   consFloat;
    char    consString[1000];
}NoAST;

/*------------------------------------*/

typedef struct darvore{
    NoArvore* raiz;
}dArvore;

typedef struct dlista{
    NoLista* inicio;
    NoLista* fim;
}dLista;

typedef struct dAST{
    NoAST* raiz;
}dAST;

/*------------------------------------*/

typedef struct atributo{
    dLista* lista;
    NoAST*  noAST;
    char    id[MAXID];
    int     tipo;

    int     consInt;
    float   consFloat;
    char*   consString;
}Atributo;




dLista*  criaLista(char* id);
dArvore* criaArvore(NoArvore* no);

NoAST*   criaFolhaID(char* id);
NoAST*   criaFolhaInt(int consInt);
NoAST*   criaFolhaIDEquals(char* id);
NoAST*   criaFolhaFloat(float consFloat);
NoAST*   criaFolhaString(char* consString);

NoAST*   criaNoAST(int operacao, NoAST* esquerda, NoAST* direita);
NoAST*   criaNoAST_IfElse(int operacao, NoAST* esquerda, NoAST* direita, NoAST* ptr3);

void     insereNaLista(dLista* desc, char* id);
dArvore* insereNaArvore(dLista* dLista, int tipo, dArvore* descArvore);
dAST*    insereNaAST(dAST* dAST, int tipo, dArvore* descArvore);

void     removeDaLista(dLista* desc);

void 	 imprimeLista(dLista* desc);
void 	 imprimeTabela(NoArvore* NoArvore);
void     imprimeHeaderTabela();

void     imprimeHeader(FILE* file, char* str);
void     imprimeVariaveis(FILE* file, int num);
void     imprimeFinal(FILE* file);

void 	 gerarCodigo(NoAST* noAST);
void	 gerarExLogRel(NoAST* noAST, int lTrue, int lFalse);

NoArvore*  verificaVariavel(dArvore* descritor, char* id);
NoAST*     verificaTipo(int operacao, NoAST* esquerda, NoAST* direita);

int 	 newLabel();

void 	 swap(int* a, int* b);
int 	 max(int a, int b);
int 	 min(int a, int b);

extern int yylex (void);
extern int yyerror (char *str);