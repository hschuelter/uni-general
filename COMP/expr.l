delim	[ \t]
ws		{delim}+
digito	[0-9]
char    [a-zA-Z]
string 	\".*\"
signal 	[\+|\-]
int 	{digito}+
float 	{digito}+(\.{digito}*(E[+-]?{digito}+)?)?
id		({char}|\_)+({char}|{digito}|\_)*

%%
{ws}	{}

"+"	    {return '+';}
"-"	    {return '-';}
"*"	    {return '*';}
"/"	    {return '/';}
"<"	    {return '<';}
">"	    {return '>';}
"<="	{return TMENORIG;}
">="	{return TMAIORIG;}
"=="	{return TIGUAL;}
"!="	{return TDIF;}
"&&"	{return TAND;}
"||"	{return TOR;}
"!"	    {return '!';}
"="		{return '=';}

"("	    {return '(';}
")"	    {return ')';}
"{"     {return '{';}
"}"     {return '}';}
"["     {return '[';}
"]"     {return ']';}
","	    {return ',';}
";"     {return ';';}

"if" 	 {return TIF;}
"else"	 {return TELSE;}
"while"  {return TWHILE;}
"return" {return TRET;}
"void"   {return TVOID;}
"int"    {return TINT;}
"float"  {return TFLOAT;}
"string" {return TSTRING;}
"\n"	 {linha++;}
"read"   {return TSCAN;}
"print"  {return TPRINT;}

{int}    {yylval.tipo = T_INT; 		yylval.consInt = atoi(yytext);    	return TVAR_INT;}
{float}	 {yylval.tipo = T_FLOAT; 	yylval.consFloat = atof(yytext);  	return TVAR_FLOAT;}
{string} {yylval.tipo = T_STRING;	yylval.consString = strdup(yytext);	return TVAR_STRING;}

{id}	 {strncpy(yylval.id, yytext, MAXID ); return TID;}
